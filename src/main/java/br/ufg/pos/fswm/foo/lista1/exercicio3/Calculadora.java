package br.ufg.pos.fswm.foo.lista1.exercicio3;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public interface Calculadora {

    double calcular(double num1, double num2);
}

package br.ufg.pos.fswm.foo.lista1.exercicio7;

import jdk.nashorn.internal.scripts.JO;

import javax.swing.*;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class Main {

    public static void main(String... args) {
        int[] vetor = new int[9];

        for(int i = 0; i < vetor.length ; i++) {
            vetor[i] = capturarInt(String.format("campo %d", i));
        }

        String resultado = new ReportadorImparesVetor().reportar(vetor);
        JOptionPane.showMessageDialog(null, resultado);
    }

    private static int capturarInt(String campo) {
        boolean isInvalido = true;
        int codigoAumento = 0;
        do {
            final String valorStr = JOptionPane.showInputDialog("Digite o(a) " + campo);
            try {
                codigoAumento = Integer.parseInt(valorStr);
                isInvalido = false;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Preencha um(a) " + campo + " válido(a)!");
            }
        } while(isInvalido);

        return codigoAumento;
    }
}

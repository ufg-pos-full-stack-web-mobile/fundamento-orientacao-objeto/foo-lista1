package br.ufg.pos.fswm.foo.lista1.exercicio9;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class SomadorMatriz {

    private final int linhas;
    private final int colunas;

    public SomadorMatriz(int linhas, int colunas) {
        this.linhas = linhas;
        this.colunas = colunas;
    }

    public int[][] somar(int[][] matriz1, int[][] matriz2) {
        final int[][] soma = new int[linhas][colunas];

        for(int i = 0 ; i < colunas ; i++) {
            for(int j = 0 ; j < linhas ; j++) {
                soma[i][j] = matriz1[i][j] + matriz2[i][j];
            }
        }

        return soma;
    }
}

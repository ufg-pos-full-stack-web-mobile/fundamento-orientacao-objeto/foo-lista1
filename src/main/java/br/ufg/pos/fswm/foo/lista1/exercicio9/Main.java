package br.ufg.pos.fswm.foo.lista1.exercicio9;

import javax.swing.*;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class Main {

    public static void main(String... args) {
        final int quantidadeLinhas = capturarInt("quantidade linhas");
        final int quantidadeColunas = capturarInt("quantidade colunas");

        JOptionPane.showMessageDialog(null, "Preencha a matriz 1");
        final int[][] matriz1 = preencherMatriz(quantidadeLinhas, quantidadeColunas);

        JOptionPane.showMessageDialog(null, "Preencha a matriz 2");
        final int[][] matriz2 = preencherMatriz(quantidadeLinhas, quantidadeColunas);

        final int[][] resultado = new SomadorMatriz(quantidadeLinhas, quantidadeColunas).somar(matriz1, matriz2);
        final String mensagem = apresentarMatriz(quantidadeLinhas, quantidadeColunas, resultado);
        JOptionPane.showMessageDialog(null, mensagem);
    }

    private static String apresentarMatriz(int quantidadeLinhas, int quantidadeColunas, int[][] resultado) {
        StringBuilder sb = new StringBuilder();
        boolean jaPassou = false;
        for(int i = 0 ; i < quantidadeColunas ; i++) {
            for(int j = 0 ; j < quantidadeLinhas ; j++) {
                if(jaPassou) {
                    sb.append(", ");
                }
                sb.append(resultado[i][j]);
                jaPassou = true;
            }
            sb.append("\n");
            jaPassou = false;
        }
        return "Resultado da Soma:\n" + sb.toString();
    }

    private static int[][] preencherMatriz(int quantidadeLinhas, int quantidadeColunas) {
        final int[][] matriz = new int[quantidadeLinhas][quantidadeColunas];

        for(int i = 0; i < quantidadeColunas; i++) {
            for(int j = 0 ; j < quantidadeLinhas ; j++) {
                matriz[i][j] = capturarInt("matriz[" + i + "][" + j + "]");
            }
        }

        return matriz;
    }

    private static int capturarInt(String campo) {
        boolean isInvalido = true;
        int codigoAumento = 0;
        do {
            final String valorStr = JOptionPane.showInputDialog("Digite o(a) " + campo);
            try {
                codigoAumento = Integer.parseInt(valorStr);
                isInvalido = false;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Preencha um(a) " + campo + " válido(a)!");
            }
        } while(isInvalido);

        return codigoAumento;
    }
}

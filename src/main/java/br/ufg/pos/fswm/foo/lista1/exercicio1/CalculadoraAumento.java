package br.ufg.pos.fswm.foo.lista1.exercicio1;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraAumento {
    public double aplicarAumento(double valor, int codigoAumento) {

        double total = valor;

        switch (codigoAumento) {
            case 1:
                total = valor * 1.1;
                break;
            case 2:
                total = valor * 1.25;
                break;
            case 3:
                total = valor * 1.3;
                break;
            case 4:
                total = valor * 1.5;
                break;
        }

        return total;
    }
}

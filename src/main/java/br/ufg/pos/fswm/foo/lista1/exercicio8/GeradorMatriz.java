package br.ufg.pos.fswm.foo.lista1.exercicio8;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class GeradorMatriz {
    private final int[] vetor;

    public GeradorMatriz(int[] vetor) {
        this.vetor = vetor;
    }


    public String gerarVetor() {
        final StringBuilder sb = new StringBuilder();
        boolean jaEntrou = false;
        for(int i = 0 ; i < vetor.length ; i++) {
            if(jaEntrou) {
                sb.append(", ");
            }
            sb.append(vetor[i]);
            jaEntrou = true;
        }
        return sb.toString();
    }

    public String gerarMatriz() {
        final StringBuilder sb = new StringBuilder();
        boolean jaEntrou;
        int posicao = 0;
        for(int i = 0 ; i < 3 ; i++) {
            jaEntrou = false;
            for(int j = 0 ; j < 3 ; j++) {
                if(jaEntrou) {
                    sb.append(", ");
                }
                sb.append(vetor[posicao++]);
                jaEntrou = true;
            }
            if(i != 2) {
                sb.append("\n");
            }
        }
        return sb.toString();
    }
}

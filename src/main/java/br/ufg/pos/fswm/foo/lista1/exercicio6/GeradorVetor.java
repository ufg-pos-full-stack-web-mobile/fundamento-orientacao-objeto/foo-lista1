package br.ufg.pos.fswm.foo.lista1.exercicio6;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class GeradorVetor {

    private final int tamanhoVetor;
    private double[] vetor;

    public GeradorVetor(int tamanhoVetor) {
        this.tamanhoVetor = tamanhoVetor;
        vetor = new double[tamanhoVetor];
    }

    public double[] gerar() {
        vetor[0] = 100.0;
        vetor[tamanhoVetor - 1] = 100.0;

        for(int i = 1; i < tamanhoVetor - 1; i++) {
            vetor[i] = Math.pow(i-1, 2) + Math.sqrt(i+1);
        }

        return vetor;
    }
}

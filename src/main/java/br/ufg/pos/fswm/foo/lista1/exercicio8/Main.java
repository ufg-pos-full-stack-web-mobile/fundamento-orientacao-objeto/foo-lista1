package br.ufg.pos.fswm.foo.lista1.exercicio8;

import br.ufg.pos.fswm.foo.lista1.exercicio7.ReportadorImparesVetor;

import javax.swing.*;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class Main {

    public static void main(String... args) {
        int[] vetor = new int[9];

        for(int i = 0; i < vetor.length ; i++) {
            vetor[i] = capturarInt(String.format("campo %d", i));
        }

        GeradorMatriz gerador = new GeradorMatriz(vetor);

        String resultado = String.format("Vetor: %s\nMatriz:\n%s", gerador.gerarVetor(), gerador.gerarMatriz());
        JOptionPane.showMessageDialog(null, resultado);
    }

    private static int capturarInt(String campo) {
        boolean isInvalido = true;
        int inteiro = 0;
        do {
            final String valorStr = JOptionPane.showInputDialog("Digite o(a) " + campo);
            try {
                inteiro = Integer.parseInt(valorStr);
                isInvalido = false;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Preencha um(a) " + campo + " válido(a)!");
            }
        } while(isInvalido);

        return inteiro;
    }
}

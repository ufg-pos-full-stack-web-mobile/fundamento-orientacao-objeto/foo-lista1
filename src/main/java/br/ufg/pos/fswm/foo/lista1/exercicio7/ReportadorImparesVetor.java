package br.ufg.pos.fswm.foo.lista1.exercicio7;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class ReportadorImparesVetor {
    public String reportar(int[] vetor) {
        final StringBuilder sb = new StringBuilder();
        boolean jaEntrou = false;

        for(int i = 0 ; i < vetor.length; i++) {
            if(vetor[i] % 2 != 0) {
                if(jaEntrou) {
                   sb.append(", ");
                }
                sb.append(String.format("%d[%d]", vetor[i], i));
                jaEntrou = true;
            }
        }
        return sb.toString();
    }
}

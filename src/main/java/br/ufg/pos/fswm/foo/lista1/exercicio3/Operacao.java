package br.ufg.pos.fswm.foo.lista1.exercicio3;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public enum Operacao {
    SOMA(1, new CalculadoraSoma()),
    SUBTRACAO(2, new CalculadoraSubtracao()),
    MULTIPLICACAO(3, new CalculadoraMultiplicacao()),
    DIVISAO(4, new CalculadoraDivisao());

    private Integer codigoOperacao;
    private Calculadora calculadora;

    Operacao(Integer codigoOperacao, Calculadora calculadora) {
        this.codigoOperacao = codigoOperacao;
        this.calculadora = calculadora;
    }

    public static Operacao fromCodigo(int codigo) {
        for(Operacao operacao : Operacao.values()) {
            if(codigo == operacao.codigoOperacao) {
                return operacao;
            }
        }
        throw new RuntimeException("Código de Operacão não válido");
    }

    public Calculadora getCalculadora() {
        return calculadora;
    }

    public void setCalculadora(Calculadora calculadora) {
        this.calculadora = calculadora;
    }
}

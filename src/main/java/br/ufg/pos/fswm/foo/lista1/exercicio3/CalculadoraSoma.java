package br.ufg.pos.fswm.foo.lista1.exercicio3;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraSoma implements Calculadora {
    public double calcular(double num1, double num2) {
        return num1 + num2;
    }
}

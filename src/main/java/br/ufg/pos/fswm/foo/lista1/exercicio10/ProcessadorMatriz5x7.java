package br.ufg.pos.fswm.foo.lista1.exercicio10;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class ProcessadorMatriz5x7 {
    private static final int LINHAS = 5;
    private static final int COLUNAS = 7;

    private final int[][] matriz;

    public ProcessadorMatriz5x7(int[][] matriz) {
        this.matriz = matriz;
    }

    public String menorElemento() {
        int menor = Integer.MAX_VALUE;
        int linha = 0;
        int coluna = 0;

        for(int i = 0 ; i < LINHAS ; i++) {
            for(int j = 0 ; j < COLUNAS ; j++) {
                if(matriz[i][j] < menor) {
                    menor = matriz[i][j];
                    linha = i;
                    coluna = j;
                }
            }
        }

        return String.format("Menor valor é %d na posição [%d][%d]", menor, linha, coluna);
    }

    public String maiorElemento() {
        int maior = Integer.MIN_VALUE;
        int linha = 0;
        int coluna = 0;

        for(int i = 0 ; i < LINHAS ; i++) {
            for(int j = 0 ; j < COLUNAS ; j++) {
                if(matriz[i][j] > maior) {
                    maior = matriz[i][j];
                    linha = i;
                    coluna = j;
                }
            }
        }

        return String.format("Maior valor é %d na posição [%d][%d]", maior, linha, coluna);
    }
}

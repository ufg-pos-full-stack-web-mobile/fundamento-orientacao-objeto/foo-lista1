package br.ufg.pos.fswm.foo.lista1.exercicio6;

import javax.swing.*;
import java.util.Locale;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class Main {

    public static void main(String... args) {
        final int tamanhoVetor = 50;
        double[] vetor = new GeradorVetor(tamanhoVetor).gerar();

        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < tamanhoVetor ; i++) {
            sb.append(String.format(Locale.US,"%.2f", vetor[i]));
            if(i != tamanhoVetor-1) {
                sb.append(", ");
            }
        }

        JOptionPane.showMessageDialog(null, sb.toString());
    }

}

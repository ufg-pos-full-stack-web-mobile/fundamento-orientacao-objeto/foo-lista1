package br.ufg.pos.fswm.foo.lista1.exercicio2;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraSalarioLiquido {

    private final CalculadoraSalarioBruto calculadoraSalarioBruto;
    private final CalculadoraINSS calculadoraINSS;
    private final CalculadoraIR calculadoraIR;

    public CalculadoraSalarioLiquido() {
        this.calculadoraSalarioBruto = new CalculadoraSalarioBruto();
        this.calculadoraINSS = new CalculadoraINSS();
        this.calculadoraIR = new CalculadoraIR();
    }

    public double calcular(double valorHora, int horasTrabalhadas) {
        return calcular(valorHora, horasTrabalhadas, 0);
    }

    public double calcular(double valorHora, int horasTrabalhadas, int numDependentes) {
        final double salarioBruto = calculadoraSalarioBruto.calcular(valorHora, horasTrabalhadas, numDependentes);
        final double inss = calculadoraINSS.calcular(salarioBruto);
        final double ir = calculadoraIR.calcular(salarioBruto);

        return salarioBruto - inss - ir;
    }
}

package br.ufg.pos.fswm.foo.lista1.exercicio2;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraINSS {
    public double calcular(double salario) {
        if(salario <= 1000.0) {
            return salario * 0.085;
        } else {
            return salario * 0.09;
        }
    }
}

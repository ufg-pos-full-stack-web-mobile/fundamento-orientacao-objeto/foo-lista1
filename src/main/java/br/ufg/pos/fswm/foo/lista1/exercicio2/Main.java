package br.ufg.pos.fswm.foo.lista1.exercicio2;

import javax.swing.*;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class Main {

    public static void main(String... args) {
        final int horasTrabalhadas = capturarInt("quantidade de horas trabalhadas");
        final double valorHora = capturarDouble("valor da hora trabalhada");
        final int quantidadeDependentes = capturarInt("quantidade de dependentes");

        final double salarioBruto = new CalculadoraSalarioBruto().calcular(valorHora, horasTrabalhadas, quantidadeDependentes);
        final double inss = new CalculadoraINSS().calcular(salarioBruto);
        final double ir = new CalculadoraIR().calcular(salarioBruto);
        final double salarioLiquido = new CalculadoraSalarioLiquido().calcular(valorHora, horasTrabalhadas, quantidadeDependentes);

        final String mensagem = String.format("Salário Bruto: R$ %.2f\n" +
                                                "INSS: R$ %.2f\n" +
                                                "IR: R$ %.2f\n" +
                                                "Salário Liquido: R$ %.2f",
                salarioBruto, inss, ir, salarioLiquido);

        JOptionPane.showMessageDialog(null, mensagem);
    }

    private static int capturarInt(String campo) {
        boolean isInvalido = true;
        int codigoAumento = 0;
        do {
            final String valorStr = JOptionPane.showInputDialog("Digite o(a) " + campo);
            try {
                codigoAumento = Integer.parseInt(valorStr);
                isInvalido = false;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Preencha um(a) " + campo + " válido(a)!");
            }
        } while(isInvalido);

        return codigoAumento;
    }

    private static double capturarDouble(String campo) {
        boolean isInvalido = true;
        double valor = 0.0;
        do {
            final String valorStr = JOptionPane.showInputDialog("Digite o(a) " + campo);
            try {
                valor = Double.parseDouble(valorStr);
                isInvalido = false;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Preencha um(a)" + campo + " válido(a)!");
            }
        } while(isInvalido);

        return valor;
    }
}

package br.ufg.pos.fswm.foo.lista1.exercicio5;

import javax.swing.*;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class Main {

    public static void main(String... args) {
        final double terrenoTotal = capturarDouble("tamanho total terreno");
        final double terrenoConstruido = capturarDouble("tamanho construido terreno");

        final double valorIptu = new CalculadoraIPTU().calcular(terrenoTotal, terrenoConstruido);

        String mensagem = String.format("O valor do IPTU desse terreno é R$ %.2f", valorIptu);
        JOptionPane.showMessageDialog(null, mensagem);
    }

    private static double capturarDouble(String campo) {
        boolean isInvalido = true;
        double valor = 0.0;
        do {
            final String valorStr = JOptionPane.showInputDialog("Digite o(a) " + campo);
            try {
                valor = Double.parseDouble(valorStr);
                isInvalido = false;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Preencha um(a)" + campo + " válido(a)!");
            }
        } while(isInvalido);

        return valor;
    }
}

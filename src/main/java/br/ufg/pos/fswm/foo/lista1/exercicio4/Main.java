package br.ufg.pos.fswm.foo.lista1.exercicio4;

import javax.swing.*;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class Main {

    public static void main(String... args) {
        final double peso = capturarDouble("peso");
        final double altura = capturarDouble("altura");

        final double imc = new CalculadoraIMC().calcular(peso, altura);
        
        final String message = String.format("O IMC é: %.2f", imc);
        JOptionPane.showMessageDialog(null, message);
    }

    private static double capturarDouble(String campo) {
        boolean isInvalido = true;
        double valor = 0.0;
        do {
            final String valorStr = JOptionPane.showInputDialog("Digite o(a) " + campo);
            try {
                valor = Double.parseDouble(valorStr);
                isInvalido = false;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Preencha um(a)" + campo + " válido(a)!");
            }
        } while(isInvalido);

        return valor;
    }
}

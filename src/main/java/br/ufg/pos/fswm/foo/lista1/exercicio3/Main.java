package br.ufg.pos.fswm.foo.lista1.exercicio3;

import javax.swing.*;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class Main {

    public static void main(String... args) {
        final double num1 = capturarDouble("número 1");
        final double num2 = capturarDouble("número 2");
        final Operacao operacao = capturaOperacao();

        final double resultado = operacao.getCalculadora().calcular(num1, num2);

        String message = String.format("Resultado da %s entre %.1f e %.1f = %.1f",
                operacao.name(), num1, num2, resultado);
        JOptionPane.showMessageDialog(null, message);
    }

    private static Operacao capturaOperacao() {
        boolean isInvalido = true;
        Operacao operacao = null;
        do {
            final String valorStr = JOptionPane.showInputDialog("Digite o código da operação\n1 - SOMA\n2 - SUBTRAÇÃO\n3 - MULTIPLICAÇÃO\n4 - DIVISÃO");
            try {
                final int codigo = Integer.parseInt(valorStr);
                operacao = Operacao.fromCodigo(codigo);
                isInvalido = false;
            } catch (RuntimeException e) {
                JOptionPane.showMessageDialog(null, "Preencha um código válido!");
            }
        } while(isInvalido);

        return operacao;
    }

    private static double capturarDouble(String campo) {
        boolean isInvalido = true;
        double valor = 0.0;
        do {
            final String valorStr = JOptionPane.showInputDialog("Digite o(a) " + campo);
            try {
                valor = Double.parseDouble(valorStr);
                isInvalido = false;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Preencha um(a) " + campo + " válido(a)!");
            }
        } while(isInvalido);

        return valor;
    }
}

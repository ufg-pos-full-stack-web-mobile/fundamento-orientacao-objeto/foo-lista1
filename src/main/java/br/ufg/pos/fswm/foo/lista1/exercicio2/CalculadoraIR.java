package br.ufg.pos.fswm.foo.lista1.exercicio2;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraIR {


    public double calcular(double salario) {
        if(salario <= 500) {
            return 0;
        } else if(salario > 500 && salario <= 1000) {
            return salario * 0.05;
        } else {
            return salario * 0.07;
        }
    }
}

package br.ufg.pos.fswm.foo.lista1.exercicio2;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraSalarioBruto {
    public double calcular(double valorHora, int horasTrabalhadas) {
        return calcular(valorHora, horasTrabalhadas, 0);
    }

    public double calcular(double valorHora, int horasTrabalhadas, int qtdDependentes) {
        return valorHora * horasTrabalhadas + qtdDependentes * 50;
    }
}

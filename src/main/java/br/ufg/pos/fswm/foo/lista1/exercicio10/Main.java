package br.ufg.pos.fswm.foo.lista1.exercicio10;

import br.ufg.pos.fswm.foo.lista1.exercicio9.SomadorMatriz;

import javax.swing.*;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class Main {

    public static void main(String... args) {
        final int quantidadeLinhas = 5;
        final int quantidadeColunas = 7;

        JOptionPane.showMessageDialog(null, "Preencha a matriz");
        final int[][] matriz = preencherMatriz(quantidadeLinhas, quantidadeColunas);

        ProcessadorMatriz5x7 processador = new ProcessadorMatriz5x7(matriz);
        final String mensagem = String.format("%s\n%s", processador.menorElemento(), processador.maiorElemento());
        JOptionPane.showMessageDialog(null, mensagem);
    }


    private static int[][] preencherMatriz(int quantidadeLinhas, int quantidadeColunas) {
        final int[][] matriz = new int[quantidadeLinhas][quantidadeColunas];

        for(int i = 0; i < quantidadeLinhas; i++) {
            for(int j = 0 ; j < quantidadeColunas ; j++) {
                matriz[i][j] = capturarInt("matriz[" + i + "][" + j + "]");
            }
        }

        return matriz;
    }

    private static int capturarInt(String campo) {
        boolean isInvalido = true;
        int codigoAumento = 0;
        do {
            final String valorStr = JOptionPane.showInputDialog("Digite o(a) " + campo);
            try {
                codigoAumento = Integer.parseInt(valorStr);
                isInvalido = false;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Preencha um(a) " + campo + " válido(a)!");
            }
        } while(isInvalido);

        return codigoAumento;
    }
}

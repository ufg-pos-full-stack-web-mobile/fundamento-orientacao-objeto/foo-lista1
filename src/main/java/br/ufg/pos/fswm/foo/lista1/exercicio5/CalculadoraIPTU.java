package br.ufg.pos.fswm.foo.lista1.exercicio5;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraIPTU {
    public double calcular(double terrenoTotal, double terrenoConstruido) {
        return terrenoConstruido * 5.0 + (terrenoTotal - terrenoConstruido) * 3.8;
    }
}

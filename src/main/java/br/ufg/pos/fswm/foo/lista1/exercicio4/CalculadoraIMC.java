package br.ufg.pos.fswm.foo.lista1.exercicio4;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraIMC {
    public double calcular(double peso, double altura) {
        return peso / Math.pow(altura, 2);
    }
}

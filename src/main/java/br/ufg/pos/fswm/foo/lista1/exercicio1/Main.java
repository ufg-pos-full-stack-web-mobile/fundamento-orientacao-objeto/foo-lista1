package br.ufg.pos.fswm.foo.lista1.exercicio1;

import javax.swing.*;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class Main {

    public static void main(String... args) {
        final double valorProduto = capturarValorProduto();
        final int codigoAumento = capturaCodigoAumento();

        final double valorFinal = new CalculadoraVendas().calcular(valorProduto, codigoAumento);

        final String mensagemFinal = String.format("O valor final do produto é R$ %.2f", valorFinal);
        JOptionPane.showMessageDialog(null, mensagemFinal);

    }

    private static int capturaCodigoAumento() {
        boolean isInvalido = true;
        int codigoAumento = 0;
        do {
            final String valorStr = JOptionPane.showInputDialog("Digite o codigo de aumento (1 a 4)");
            try {
                codigoAumento = Integer.parseInt(valorStr);
                if(codigoAumento < 1 || codigoAumento > 4) {
                    JOptionPane.showMessageDialog(null, "Codigo inválido.");
                } else {
                    isInvalido = false;
                }
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Preencha um codigo válido!");
            }
        } while(isInvalido);

        return codigoAumento;
    }

    private static double capturarValorProduto() {
        boolean isInvalido = true;
        double valor = 0.0;
        do {
            final String valorStr = JOptionPane.showInputDialog("Digite o valor do produto");
            try {
                valor = Double.parseDouble(valorStr);
                isInvalido = false;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Preencha um valor válido!");
            }
        } while(isInvalido);

        return valor;
    }
}

package br.ufg.pos.fswm.foo.lista1.exercicio1;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraImpostos {
    public double calcular(double valor) {
        if(valor >= 1000 && valor <= 5000) {
            return valor * 1.01;
        } else if(valor > 5000 && valor <= 10000) {
            return valor * 1.02;
        } else {
            return valor * 1.03;
        }
    }
}

package br.ufg.pos.fswm.foo.lista1.exercicio1;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraVendas {

    private final CalculadoraAumento calculadoraAumento;
    private final CalculadoraImpostos calculadoraImpostos;

    public CalculadoraVendas() {
        this.calculadoraAumento = new CalculadoraAumento();
        this.calculadoraImpostos = new CalculadoraImpostos();
    }

    public double calcular(double valorProduto, int codigoAumento) {
        valorProduto = calculadoraAumento.aplicarAumento(valorProduto, codigoAumento);
        valorProduto = calculadoraImpostos.calcular(valorProduto);
        return valorProduto;
    }
}

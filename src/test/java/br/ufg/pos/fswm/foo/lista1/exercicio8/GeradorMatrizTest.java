package br.ufg.pos.fswm.foo.lista1.exercicio8;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class GeradorMatrizTest {

    private GeradorMatriz sut;

    private int[] vetor = {154, 157, 65, 165, 89761, 1467, 497, 941, 641};

    @Before
    public void setUp() throws Exception {
        sut = new GeradorMatriz(vetor);
    }

    @Test
    public void deve_retornar_vetor_em_formato_string() throws Exception {
        final String esperado = "154, 157, 65, 165, 89761, 1467, 497, 941, 641";
        final String retornado = sut.gerarVetor();

        assertEquals(esperado, retornado);
    }

    @Test
    public void deve_retornar_matriz_em_formato_string() throws Exception {
        final String esperado = "154, 157, 65\n165, 89761, 1467\n497, 941, 641";
        final String retornado = sut.gerarMatriz();

        assertEquals(esperado, retornado);
    }
}

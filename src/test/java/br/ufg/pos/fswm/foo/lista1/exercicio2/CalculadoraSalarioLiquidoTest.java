package br.ufg.pos.fswm.foo.lista1.exercicio2;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraSalarioLiquidoTest {

    public static final double DELTA = 0.001;
    private CalculadoraSalarioLiquido sut;

    @Before
    public void setUp() throws Exception {
        sut = new CalculadoraSalarioLiquido();
    }

    @Test
    public void deve_ser_possivel_calcular_salario_liquido_atraves_das_horas_trabalhadas_valor_hora() throws Exception {
        final double valorHora = 15.0;
        final int horasTrabalhadas = 160;
        final double salarioLiquidoEsperado = 2016.0;
        final double salarioLiquido = sut.calcular(valorHora, horasTrabalhadas);

        assertEquals(salarioLiquidoEsperado, salarioLiquido, DELTA);
    }

    @Test
    public void deve_ser_possivel_calcular_salario_liquido_atraves_das_horas_trabalhadas_valor_hora_e_quantidade_dependentes() throws Exception {
        final double valorHora = 15.0;
        final int horasTrabalhadas = 160;
        final int numDependentes = 2;
        final double salarioLiquidoEsperado = 2100.0;
        final double salarioLiquido = sut.calcular(valorHora, horasTrabalhadas, numDependentes);

        assertEquals(salarioLiquidoEsperado, salarioLiquido, DELTA);
    }
}

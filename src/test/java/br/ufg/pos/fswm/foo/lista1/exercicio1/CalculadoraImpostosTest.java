package br.ufg.pos.fswm.foo.lista1.exercicio1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraImpostosTest {

    public static final double DELTA = 0.001;
    private CalculadoraImpostos sut;

    @Test
    public void deve_aplicar_imposto_de_1_porcento_para_produtos_com_valor_entre_1000_e_5000_reais() throws Exception {
        sut = new CalculadoraImpostos();

        assertEquals(1010.0, sut.calcular(1000.0), DELTA);
        assertEquals(2020.0, sut.calcular(2000.0), DELTA);
        assertEquals(3030.0, sut.calcular(3000.0), DELTA);
        assertEquals(4040.0, sut.calcular(4000.0), DELTA);
        assertEquals(5050.0, sut.calcular(5000.0), DELTA);
    }

    @Test
    public void deve_aplicar_imposto_de_2_porcento_para_produtos_com_valor_entre_5001_e_10000_reais() throws Exception {
        sut = new CalculadoraImpostos();

        assertEquals(5101.02, sut.calcular(5001), DELTA);
        assertEquals(6120.0, sut.calcular(6000), DELTA);
        assertEquals(7140.0, sut.calcular(7000), DELTA);
        assertEquals(8160.0, sut.calcular(8000), DELTA);
        assertEquals(9180.0, sut.calcular(9000), DELTA);
    }

    @Test
    public void deve_aplicar_imposto_de_3_porcento_para_produtos_com_valor_acima_de_10000_reais() throws Exception {
        sut = new CalculadoraImpostos();

        assertEquals(10300.0103, sut.calcular(10000.01), DELTA);
        assertEquals(11330.0, sut.calcular(11000.0), DELTA);
    }
}

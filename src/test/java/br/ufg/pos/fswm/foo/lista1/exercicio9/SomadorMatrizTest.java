package br.ufg.pos.fswm.foo.lista1.exercicio9;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class SomadorMatrizTest {

    public static final int LINHAS = 3;
    public static final int COLUNAS = 3;

    private SomadorMatriz sut;

    @Before
    public void setUp() throws Exception {
        sut = new SomadorMatriz(LINHAS, COLUNAS);
    }

    @Test
    public void deve_somar_duas_matrizes() throws Exception {
        int[][] matriz1 = {{2,3,4},{5,6,7},{8,9,10}};
        int[][] matriz2 = {{5,7,9},{10,16,17},{28,29,210}};
        int[][] esperado = {{7,10,13},{15,22,24},{36,38,230}};

        sut.somar(matriz1, matriz2);

    }
}

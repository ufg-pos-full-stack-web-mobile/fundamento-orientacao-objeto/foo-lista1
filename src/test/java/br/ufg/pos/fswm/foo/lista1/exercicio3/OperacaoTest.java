package br.ufg.pos.fswm.foo.lista1.exercicio3;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class OperacaoTest {

    @Test
    public void deve_retornar_operacao_SOMA_para_codigo_1() throws Exception {
        assertEquals(Operacao.SOMA, Operacao.fromCodigo(1));
    }

    @Test
    public void deve_retornar_operacao_SUBTRACAO_para_codigo_2() throws Exception {
        assertEquals(Operacao.SUBTRACAO, Operacao.fromCodigo(2));
    }

    @Test
    public void deve_retornar_operacao_MULTIPLICACAO_para_codigo_3() throws Exception {
        assertEquals(Operacao.MULTIPLICACAO, Operacao.fromCodigo(3));
    }

    @Test
    public void deve_retornar_operaca_DIVISAO_para_codigo_4() throws Exception {
        assertEquals(Operacao.DIVISAO, Operacao.fromCodigo(4));
    }

    @Test
    public void deve_retornar_CalculadoraSoma_para_codigo_1() throws Exception {
        Operacao operacao = Operacao.fromCodigo(1);
        Calculadora calculadora = operacao.getCalculadora();
        assertTrue(calculadora instanceof CalculadoraSoma);
    }

    @Test
    public void deve_retornar_CalculadoraSubtracao_para_codigo_2() throws Exception {
        Operacao operacao = Operacao.fromCodigo(2);
        Calculadora calculadora = operacao.getCalculadora();
        assertTrue(calculadora instanceof CalculadoraSubtracao);
    }

    @Test
    public void deve_retornar_CalculadoraMultiplicacao_para_codigo_3() throws Exception {
        Operacao operacao = Operacao.fromCodigo(3);
        Calculadora calculadora = operacao.getCalculadora();
        assertTrue(calculadora instanceof CalculadoraMultiplicacao);
    }

    @Test
    public void deve_retornar_CalculadoraDivisao_para_codigo_4() throws Exception {
        Operacao operacao = Operacao.fromCodigo(4);
        Calculadora calculadora = operacao.getCalculadora();
        assertTrue(calculadora instanceof CalculadoraDivisao);
    }
}

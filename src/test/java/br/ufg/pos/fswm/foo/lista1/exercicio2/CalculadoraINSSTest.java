package br.ufg.pos.fswm.foo.lista1.exercicio2;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraINSSTest {

    public static final double DELTA = 0.001;
    private CalculadoraINSS sut;

    @Before
    public void setUp() throws Exception {
        sut = new CalculadoraINSS();
    }

    private void executarCalculo(double salario, double esperado) {
        double inss = sut.calcular(salario);
        assertEquals(esperado, inss, DELTA);
    }

    @Test
    public void deve_calcular_oito_e_meio_porcento_de_INSS_para_salarios_menores_que_1000() throws Exception {
        executarCalculo(750.0, 63.75);
    }

    @Test
    public void deve_calcular_oito_e_meio_porcento_de_INSS_para_salarios_iguais_a_1000() throws Exception {
        executarCalculo(1000.0, 85.0);
    }

    @Test
    public void deve_calcular_nove_porcento_de_INSS_para_salarios_maiores_que_1000() throws Exception {
        executarCalculo(1500.0, 135.0);
    }
}

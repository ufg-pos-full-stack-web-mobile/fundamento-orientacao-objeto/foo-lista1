package br.ufg.pos.fswm.foo.lista1.exercicio4;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraIMCTest {

    public static final double DELTA = 0.0001;
    private CalculadoraIMC sut;

    @Before
    public void setUp() throws Exception {
        sut = new CalculadoraIMC();
    }

    @Test
    public void deve_ser_possivel_calcular_IMC_de_pessoa() throws Exception {
        final double esperado = 22.249134948;
        final double imc = sut.calcular(64.3, 1.70);

        assertEquals(esperado, imc, DELTA);
    }
}

package br.ufg.pos.fswm.foo.lista1.exercicio2;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraSalarioBrutoTest {

    public static final double DELTA = 0.001;
    private CalculadoraSalarioBruto sut;

    @Before
    public void setUp() throws Exception {
        sut = new CalculadoraSalarioBruto();
    }

    @Test
    public void deve_calcular_salario_bruto_a_partir_das_horas_trabalhadas_e_do_valor_da_hora_trabalhada() throws Exception {
        final double valorHora = 15.0;
        final int horasTrabalhadas = 160;
        final double esperado = 2400.0;

        final double salarioBruto = sut.calcular(valorHora, horasTrabalhadas);
        assertEquals(esperado, salarioBruto, DELTA);
    }

    @Test
    public void deve_calcular_salario_bruto_a_partir_das_horas_trabalhadas_do_valor_da_hora_e_quantidade_de_dependentes() throws Exception {
        final double valorHora = 15.0;
        final int horasTrabalhadas = 160;
        final int qtdDependentes = 1;
        final double esperado = 2450.0;

        final double salarioBruto = sut.calcular(valorHora, horasTrabalhadas, qtdDependentes);
        assertEquals(esperado, salarioBruto, DELTA);
    }

    @Test
    public void deve_calcular_salario_bruto_a_partir_das_horas_trabalhadas_do_valor_da_hora_e_com_mais_de_um_dependente() throws Exception {
        final double valorHora = 15.0;
        final int horasTrabalhadas = 160;
        final int qtdDependentes = 3;
        final double esperado = 2550.0;

        final double salarioBruto = sut.calcular(valorHora, horasTrabalhadas, qtdDependentes);
        assertEquals(esperado, salarioBruto, DELTA);
    }
}

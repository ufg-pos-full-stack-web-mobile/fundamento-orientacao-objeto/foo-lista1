package br.ufg.pos.fswm.foo.lista1.exercicio10;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class ProcessadorMatriz5x7Test {

    private ProcessadorMatriz5x7 sut;
    private int[][] matriz;

    @Before
    public void setUp() throws Exception {
        int[][] matriz = {{321, 1654, 1664, 481, 8167, 416, -4513},
                {65491, -4545, 54, -4613, 1687, -1643, 16751},
                {56197, 6516, 4948, -51973, 5167, 1631, -46481},
                {981654, 6514, -9154, 61643, 91637, -59743, 1684},
                {6541, 164, 18767, 684341, 8173, 19761, 681973}};

        sut = new ProcessadorMatriz5x7(matriz);

    }

    @Test
    public void deve_informar_o_menor_elemento_da_matriz_e_sua_posicao() throws Exception {
        final String esperado = "Menor valor é -59743 na posição [3][5]";
        final String calculado = sut.menorElemento();

        assertEquals(esperado, calculado);
    }

    @Test
    public void deve_informar_o_maior_elemento_da_matriz_e_sua_posicao() throws Exception {
        final String esperado = "Maior valor é 981654 na posição [3][0]";
        final String calculado = sut.maiorElemento();

        assertEquals(esperado, calculado);
    }
}

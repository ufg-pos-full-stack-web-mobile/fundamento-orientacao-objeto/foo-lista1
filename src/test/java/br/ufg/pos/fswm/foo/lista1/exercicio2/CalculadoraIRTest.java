package br.ufg.pos.fswm.foo.lista1.exercicio2;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraIRTest {

    public static final double DELTA = 0.001;
    private CalculadoraIR sut;

    @Before
    public void setUp() throws Exception {
        sut = new CalculadoraIR();
    }

    @Test
    public void deve_isentar_imposto_de_renda_para_salario_menor_que_500() throws Exception {
        executarCalculo(250.0, 0.0);
    }

    @Test
    public void deve_isentar_imposto_de_renda_para_salario_de_500() throws Exception {
        executarCalculo(500.0, 0.0);
    }

    @Test
    public void deve_incidir_5_porcento_de_imposto_de_renda_para_salarios_maiores_que_500() throws Exception {
        executarCalculo(500.01, 25.0005);
        executarCalculo(700.0, 35.0);
    }

    @Test
    public void deve_insidir_7_porcento_de_imposto_de_renda_para_salarios_maiores_que_1000() throws Exception {
        executarCalculo(1000.01, 70.0007);
        executarCalculo(1500.0, 105.0);
    }

    private void executarCalculo(double salario, double irEsperado) {
        assertEquals(irEsperado, sut.calcular(salario), DELTA);
    }


}

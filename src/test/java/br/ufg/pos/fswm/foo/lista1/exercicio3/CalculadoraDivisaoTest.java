package br.ufg.pos.fswm.foo.lista1.exercicio3;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraDivisaoTest {

    private Calculadora sut;

    @Before
    public void setUp() throws Exception {
        sut = new CalculadoraDivisao();
    }

    @Test
    public void deve_ser_possivel_dividir_dois_numeros() throws Exception {
        final double esperado = sut.calcular(15.0, 3.0);
        assertEquals(5.0, esperado, 0.001);
    }
}

package br.ufg.pos.fswm.foo.lista1.exercicio3;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraSubtracaoTest {

    private Calculadora sut;

    @Before
    public void setUp() throws Exception {
        sut = new CalculadoraSubtracao();
    }

    @Test
    public void deve_realizar_a_subtracao_de_dois_numeros() throws Exception {
        final double calculado = sut.calcular(150, 130);
        assertEquals(20, calculado, 0.0001);
    }
}

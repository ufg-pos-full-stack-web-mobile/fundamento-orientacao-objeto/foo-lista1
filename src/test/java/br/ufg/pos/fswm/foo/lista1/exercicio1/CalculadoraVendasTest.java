package br.ufg.pos.fswm.foo.lista1.exercicio1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraVendasTest {

    public static final double DELTA = 0.001;
    private CalculadoraVendas sut;


    @Before
    public void setUp() throws Exception {
        sut = new CalculadoraVendas();
    }

    @Test
    public void deve_calcular_valor_final_a_partir_de_valor_de_produto_e_codigo_aumento_1() throws Exception {
        executarCalculo(1000.0, 1, 1111.0);
    }

    @Test
    public void deve_calcular_valor_final_a_partir_de_valor_de_produto_e_codigo_aumento_2() throws Exception {
        executarCalculo(5000, 2, 6375.0);
    }

    @Test
    public void deve_calcular_valor_final_a_partir_de_valor_de_produto_e_codigo_aumento_3() throws Exception {
        executarCalculo(10000, 3, 13390.0);
    }

    @Test
    public void deve_calcular_valor_final_a_partir_de_valor_de_produto_e_codigo_aumento_4() throws Exception {
        executarCalculo(1000.0, 4, 1515.0);
    }

    private void executarCalculo(double valorProduto, int codigoAumento, double expected) {
        double valorFinal = sut.calcular(valorProduto, codigoAumento);

        assertEquals(expected, valorFinal, DELTA);
    }

}

package br.ufg.pos.fswm.foo.lista1.exercicio3;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraSomaTest {

    public static final double DELTA = 0.001;
    private Calculadora sut;

    @Before
    public void setUp() throws Exception {
        sut = new CalculadoraSoma();
    }

    @Test
    public void deve_executar_soma_de_dois_numeros() throws Exception {
        final double resultado = sut.calcular(195.3, 145.9);
        final double esperado = 341.2;

        assertEquals(esperado, resultado, DELTA);
    }
}

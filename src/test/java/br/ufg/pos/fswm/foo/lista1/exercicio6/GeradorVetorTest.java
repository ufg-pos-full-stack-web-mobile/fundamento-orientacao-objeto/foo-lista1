package br.ufg.pos.fswm.foo.lista1.exercicio6;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class GeradorVetorTest {

    public static final double DELTA = 0.000000001;
    private GeradorVetor sut;

    @Before
    public void setUp() throws Exception {
        sut = new GeradorVetor(50);
    }

    @Test
    public void o_primeiro_elemento_do_vetor_deve_ser_100() throws Exception {
        double[] vetor = sut.gerar();

        assertEquals(100.0, vetor[0], DELTA);
    }

    @Test
    public void o_ultimo_elemento_do_vetor_deve_ser_100() throws Exception {
        double[] vetor = sut.gerar();

        assertEquals(100.0, vetor[49], DELTA);
    }

    @Test
    public void o_segundo_elemento_do_vetor_deve_ser_a_soma_do_quadrado_do_indice_anterior_com_a_raiz_quadrada_do_indice_do_proximo_elemento() throws Exception {
        double[] vetor = sut.gerar();

        assertEquals(1.414213562, vetor[1], DELTA);
    }

    @Test
    public void validar_o_terceiro_elemento_do_vetor() throws Exception {
        double[] vetor = sut.gerar();

        assertEquals(2.732050808, vetor[2], DELTA);
    }

    @Test
    public void validar_o_antepenultimo_elemento_do_vetor() throws Exception {
        double[] vetor = sut.gerar();

        assertEquals(2216, vetor[48], DELTA);
    }
}

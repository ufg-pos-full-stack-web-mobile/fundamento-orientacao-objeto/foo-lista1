package br.ufg.pos.fswm.foo.lista1.exercicio1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraAumentoTest {

    public static final double DELTA = 0.001;
    public static final double VALOR = 1000.0;

    private CalculadoraAumento sut;

    @Before
    public void setUp() throws Exception {
        sut = new CalculadoraAumento();
    }

    @Test
    public void deve_aplicar_aumento_de_10_porcento_com_codigo_aumento_1() throws Exception {
        assertEquals(1100, sut.aplicarAumento(VALOR, 1), DELTA);
    }

    @Test
    public void deve_aplicar_aumento_de_25_porcento_com_codigo_aumento_2() throws Exception {
        assertEquals(1250.0, sut.aplicarAumento(VALOR, 2), DELTA);
    }

    @Test
    public void deve_aplica_aumento_de_30_porcento_com_codigo_aumento_3() throws Exception {
        assertEquals(1300.0, sut.aplicarAumento(VALOR, 3), DELTA);
    }

    @Test
    public void deve_aplicar_aumento_de_50_porcento_com_codigo_aumento_4() throws Exception {
        assertEquals(1500, sut.aplicarAumento(VALOR, 4), DELTA);
    }
}

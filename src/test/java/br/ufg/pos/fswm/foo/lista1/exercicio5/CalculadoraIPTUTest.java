package br.ufg.pos.fswm.foo.lista1.exercicio5;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraIPTUTest {

    public static final double DELTA = 0.001;
    private CalculadoraIPTU sut;

    @Before
    public void setUp() throws Exception {
        sut = new CalculadoraIPTU();
    }

    private void executarCalculo(double terrenoTotal, double terrenoConstruido, double esperado) {
        final double calculado = sut.calcular(terrenoTotal, terrenoConstruido);
        assertEquals(esperado, calculado, DELTA);
    }

    @Test
    public void deve_calcular_imposto_sobre_um_terreno_completamente_construido() throws Exception {
        final double terrenoTotal = 50.0;
        final double terrenoConstruido = 50.0;
        final double esperado = 250.0;

        executarCalculo(terrenoTotal, terrenoConstruido, esperado);
    }

    @Test
    public void deve_calcular_imposto_sobre_um_terreno_parcialmente_construido() throws Exception {
        final double terrenoTotal = 50.0;
        final double terrenoConstruido = 30.0;
        final double esperado = 226.0;

        executarCalculo(terrenoTotal, terrenoConstruido, esperado);
    }

    @Test
    public void deve_calcular_imposto_sobre_um_terreno_nao_construido() throws Exception {
        final double terrenoTotal = 50.0;
        final double terrenoConstruido = 0.0;
        final double esperado = 190.0;

        executarCalculo(terrenoTotal, terrenoConstruido, esperado);
    }
}

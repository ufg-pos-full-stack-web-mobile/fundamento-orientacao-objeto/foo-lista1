package br.ufg.pos.fswm.foo.lista1.exercicio7;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class ReportadorImparesVetorTest {

    private ReportadorImparesVetor sut;

    @Before
    public void setUp() throws Exception {
        sut = new ReportadorImparesVetor();
    }

    @Test
    public void deve_reportar_impares_dentro_de_um_vetor_de_9_posicoes() throws Exception {
        int[] vetor = {155, 212, 248, 68413, 21984, 549198, 965761, 515, 1684};

        final String esperado = "155[0], 68413[3], 965761[6], 515[7]";
        final String calculado = sut.reportar(vetor);

        assertEquals(esperado, calculado);
    }
}

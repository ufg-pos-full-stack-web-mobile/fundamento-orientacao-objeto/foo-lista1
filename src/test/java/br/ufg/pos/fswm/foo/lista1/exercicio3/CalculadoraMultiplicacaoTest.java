package br.ufg.pos.fswm.foo.lista1.exercicio3;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraMultiplicacaoTest {

    private Calculadora sut;

    @Before
    public void setUp() throws Exception {
        sut = new CalculadoraMultiplicacao();
    }

    @Test
    public void deve_ser_possivel_multiplicar_dois_numeros() throws Exception {
        final double resultado = sut.calcular(5.0, 3.0);
        assertEquals(15.0, resultado, 0.001);
    }
}
